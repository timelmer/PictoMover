﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;

namespace PictoMover
{
    public partial class StartupForm : Form
    {
        private string tempDirectory = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "PictoMover\\Temp");

        private Dictionary<int, string> months = new Dictionary<int, string>
        {
            { 1, "Jan" },
            { 2, "Feb" },
            { 3, "Mar" },
            { 4, "Apr" },
            { 5, "May" },
            { 6, "Jun" },
            { 7, "Jul" },
            { 8, "Aug" },
            { 9, "Sep" },
            { 10, "Oct" },
            { 11, "Nov" },
            { 12, "Dec" }
        };

        public StartupForm()
        {
            InitializeComponent();
            sourceTextBox.Text = Properties.Settings.Default.sourcePath;
            destinationTextBox.Text = Properties.Settings.Default.destinationPath;
            folderNameComboBox.Text = Properties.Settings.Default.folderName;
        }

        private void browseSourceButton_Click(object sender, EventArgs e)
        {
            folderBrowserDialog.SelectedPath = Properties.Settings.Default.sourcePath;
            if (folderBrowserDialog.ShowDialog(this) == DialogResult.OK)
                sourceTextBox.Text = folderBrowserDialog.SelectedPath;
        }

        private void browseDestinationButton_Click(object sender, EventArgs e)
        {
            folderBrowserDialog.SelectedPath = Properties.Settings.Default.destinationPath;
            if (folderBrowserDialog.ShowDialog(this) == DialogResult.OK)
                destinationTextBox.Text = folderBrowserDialog.SelectedPath;
        }

        private void startButton_Click(object sender, EventArgs e)
        {
            if (!Directory.Exists(sourceTextBox.Text))
            {
                MessageBox.Show("The selected source directory does not exist!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (!Directory.Exists(destinationTextBox.Text))
            {
                MessageBox.Show("The selected destination directory does not exist!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            Properties.Settings.Default.sourcePath = sourceTextBox.Text;
            Properties.Settings.Default.destinationPath = destinationTextBox.Text;
            Properties.Settings.Default.folderName = folderNameComboBox.Text;
            Properties.Settings.Default.Save();
            Review();
            Sort();
            Close();
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void Review()
        {
            Directory.CreateDirectory(tempDirectory);

            MessageBox.Show("All photos in the source directory will be moved to a temporary folder for review. Please delete any unwanted photos.", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);

            // Check if temp directory empty
            if (Directory.GetFiles(tempDirectory).GetLength(0) != 0)
                MessageBox.Show("There are unreviewed photos present. This may be a result of a prior application crash. Please review these photos and rerun the program.", "Photos Already Present", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            else
            {
                // If so, move/copy from source
                string[] photos = Directory.GetFiles(Properties.Settings.Default.sourcePath);

                progressBar.Maximum = photos.Length;

                for (int i = 0; i < photos.Length; i++)
                {
                    progressBar.Value = i + 1;

                    if (Properties.Settings.Default.move)
                        File.Move(photos[i], Path.Combine(tempDirectory, Path.GetFileName(photos[i])));
                    else
                        File.Copy(photos[i], Path.Combine(tempDirectory, Path.GetFileName(photos[i])));
                }
            }

            Process.Start("explorer.exe", tempDirectory);
        }

        private void Sort()
        {
            progressBar.Value = 0;
            MessageBox.Show("Click OK when finished reviewing photos.", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
            string[] photos = Directory.GetFiles(tempDirectory);

            progressBar.Maximum = photos.Length;

            for (int i = 0; i < photos.Length; i++)
            {
                progressBar.Value = i + 1;

                DateTime modifyDate = File.GetLastWriteTime(photos[i]);
                string sortPath = Path.Combine(Properties.Settings.Default.destinationPath, 
                    (modifyDate.Year + "-" + (modifyDate.Month > 9 ? modifyDate.Month.ToString() : "0" + modifyDate.Month)) + 
                    (Properties.Settings.Default.folderName.Equals("YYYY-MM (Mmm)") ? " (" + months[modifyDate.Month] + ")" : ""));
                Directory.CreateDirectory(sortPath);

                try
                {
                    File.Move(photos[i], Path.Combine(Properties.Settings.Default.destinationPath, sortPath, Path.GetFileName(photos[i])));
                }
                catch (IOException e)
                {
                    switch (MessageBox.Show(e.Message, "IO Error", MessageBoxButtons.AbortRetryIgnore, MessageBoxIcon.Error))
                    {
                        case DialogResult.Abort:
                            Close();
                            break;
                        case DialogResult.Retry:
                            i--;
                            continue;
                        case DialogResult.Ignore:
                            continue;
                        default:
                            MessageBox.Show("An unrecoverable error 0x1 has occured. The program will now close.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            Close();
                            break;
                    }
                }
            }

            switch (MessageBox.Show("Done! Do you wish to open the sorted folder?", "Info", MessageBoxButtons.YesNo, MessageBoxIcon.Information))
            {
                case DialogResult.Yes:
                    Process.Start("explorer.exe", Properties.Settings.Default.destinationPath);
                    Close();
                    break;
                case DialogResult.No:
                    Close();
                    break;
                default:
                    MessageBox.Show("An unrecoverable error 0x2 has occured. The program will now close.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Close();
                    break;
            }
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start("https://www.awicons.com/free-icons/toolbar-icons/vista-stock-icons-by-lokas-software/");
        }
    }
}
